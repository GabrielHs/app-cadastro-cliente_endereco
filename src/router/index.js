import Vue from "vue";
import Router from "vue-router";
import Home from "../views/Home.vue";

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: "/",
      nome: "/",
      component: Home,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      nome: "login",
      component: () => import("../views/Login.vue"),
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: "/cadastro-user",
      nome: "cadastro-user",
      component: () => import("../views/CreateUser.vue"),
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: "/perfil/:id",
      nome: "perfil",
      component: () => import("../views/Profile.vue"),
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/cadastro/endereco",
      nome: "endereco",
      component: () => import("../views/Endereco.vue"),
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (sessionStorage.getItem("jwt") == null) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (sessionStorage.getItem("email") == null) {
      next();
    } else {
      next({ name: "/" });
    }
  } else {
    next();
  }
});

export default router;
