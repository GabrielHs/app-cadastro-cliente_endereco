import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
// import moment from "moment";
import * as VueGoogleMaps from 'vue2-google-maps'
import GmapCluster from 'vue2-google-maps/dist/components/cluster'
import VueMoment from "vue-moment";
import VueToastr from "vue-toastr";
import VueMask from "v-mask";

Vue.config.productionTip = false;

Vue.component('GmapCluster', GmapCluster)

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.use(VueGoogleMaps, {
  load: {
    key: `Chave do google maps`,
    libraries: 'places'
  },
  installComponents: true
})
Vue.use(VueMoment);
Vue.use(VueMask);
Vue.prototype.$axios = axios;
Vue.use(VueToastr, {
  defaultTimeout: 3000,
  defaultProgressBar: false,
  defaultProgressBarValue: 0,
  // defaultType: "error",
  defaultPosition: "toast-bottom-right",
  defaultCloseOnHover: false,
  // defaultStyle: { "background-color": "red" },
  defaultClassNames: ["animated", "zoomInUp"],
  clickClose: true,
});

// this.$toastr.s("SUCCESS MESSAGE", "Success Toast Title");
// this.$toastr.e("ERRROR MESSAGE");
// this.$toastr.w("WARNING MESSAGE");
// this.$toastr.i("INFORMATION MESSAGE");

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
