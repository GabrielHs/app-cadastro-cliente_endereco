# app-cliente-endereco

## Instalar dependencias apos projeto clonado
```
yarn install
```

### Iniciar projeto 
```
yarn serve
```

### Fazer deploy pra produção
```
yarn build
```

### Inserir chave do google maps
- Ir no arquivo main.js e na linha 22 informar a chave do google maps

### Esse projeto possui backend 
Clonar [repositorio](https://bitbucket.org/GabrielHs/cadastro_cliente_enderco/src/master/).
